# Copyright (c) 2012-2015 The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

ENABLE_DEFAULT_BOOTANIMATION:=true
ENABLE_LIBRECOVERY ?= true
ENABLE_RADVD := true
include gonk-misc/b2g.mk
include external/moztt/fonts.mk
TARGET_PROVIDES_INIT_RC :=
PRODUCT_PACKAGES := $(filter-out rilproxy oom-msg-logger,$(PRODUCT_PACKAGES))

PRODUCT_PROPERTY_OVERRIDES += \
  ro.display.colorfill=1 \
  ro.moz.ril.emergency_by_default=true \
  ro.moz.fm.noAnalog=true \
  ro.moz.bluetooth.backend=bluetoothd

ifneq ($(PLATFORM_SDK_VERSION), 15)
PRODUCT_PROPERTY_OVERRIDES += ro.moz.devinputjack=1
endif

ifneq ($(filter user userdebug, $(TARGET_BUILD_VARIANT)),)
USE_JSMIN = 1
endif


#
# Disable parallel make for gaia to improve build stability.
#
# It would be nice to use $(ANDROID_BUILD_TOP) instead of '%', but the |my-dir|
# macro used to compute the LOCAL_PATH macro in gaia/Android.mk has been
# observed to produce different results from $(ANDROID_BUILD_TOP) when there
# is a symbolic link in the path to the build root.
#
%/gaia/profile.tar.gz: MAKE:=$(MAKE) -j1
gaia/profile.tar.gz: MAKE:=$(MAKE) -j1


export B2G_DEBUG

# .mk files useful for building xpcom components
CLEAR_XPCOM_VARS:=$(dir $(firstword $(MAKEFILE_LIST)))clear_xpcom_vars.mk
BUILD_XPCOM:=$(dir $(firstword $(MAKEFILE_LIST)))xpcom.mk


# Define JS_BINARY to enable minification of Gecko chrome javscript files
#
# JS_BINARY is used by the Gecko build system to verify that minified chrome
# Javscript code is still correct.  Unfortunately the normal "js binary" is not
# a part of the source tree and it would be undesirable to add it as a
# dependency.  This block instead adapts the xpcshell Javscript runtime, used by
# build Gaia, for this purpose.
#
export JS_BINARY=$(abspath out/js_binary.sh)
ifeq ($(wildcard $(JS_BINARY)),)

  XPCSHELL=$(shell $(MAKE) -s -C gaia xpcshell_sdk)
  ifeq ($(wildcard $(XPCSHELL)),)
    $(error Unabled to find xpcshell.  Best guess: $(XPCSHELL))
  endif
  RUN_MOZILLA=$(dir $(XPCSHELL))run-mozilla.sh
  ifeq ($(wildcard $(RUN_MOZILLA)),)
    $(error Unabled to find run-mozilla.sh.  Best guess: $(RUN_MOZILLA))
  endif

  # This specialized JS_BINARY is expected to receive three arguments:
  #   1 - Validation script
  #   2 - Javacript file 1
  #   3 - Javacript file 2
  $(shell mkdir -p $(dir $(JS_BINARY)))
  $(shell echo '#!/bin/bash' >> $(JS_BINARY))
  $(shell echo 'exec $(RUN_MOZILLA) $(XPCSHELL) -e "scriptArgs=[$$2, $$3];" -f $$1' >> $(JS_BINARY))
  $(shell chmod +x $(JS_BINARY))
endif


#
# Multilocale support.
#
# Define the B2G_LANGUAGE_PACK variable in the environment before a build
# to select a preconfigured collection of locales.
#
# Alternatively manually define the LOCALES_FILE, GAIA_DEFAULT_LOCALE, and
# MOZ_CHROME_MULTILOCALE environment variables to create new combinations
# without modifying this file, if desired. More information about these
# environment variables may be found at:
#   https://developer.mozilla.org/en-US/docs/Mozilla/Firefox_OS/Building#Building_multilocale
#

ifeq ($(strip $(B2G_LANGUAGE_PACK)),basecamp)
# Locales to enable for Gaia
LOCALES_FILE=locales/languages_basecamp.json
# Default Gaia locale
GAIA_DEFAULT_LOCALE=en-US
# Locales to enable for Gecko
MOZ_CHROME_MULTILOCALE=pt-BR es-ES
endif

ifeq ($(strip $(B2G_LANGUAGE_PACK)),shira)
# Locales to enable for Gaia
LOCALES_FILE=locales/languages_shira.json
# Default Gaia locale
GAIA_DEFAULT_LOCALE=en-US
# Locales to enable for Gecko
MOZ_CHROME_MULTILOCALE=cs de hr hu pl sr ro
endif

# Target-specific Gecko configuration
# Enable DSP-based EC/NS for webRTC
GECKO_CONFIGURE_ARGS+=--enable-hardware-aec-ns
# Disable Mozilla's RIL and Location modules
GECKO_CONFIGURE_ARGS+=--disable-mozril-geoloc

export PATH
export PYTHONPATH
