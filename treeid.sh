#!/bin/bash
#
# Attempt to identify the tree in use.  On success one or more
# tree identifiers are output to stdout.
#
# Copyright (c) 2012-2015, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


if [[ ! ( -f build/envsetup.sh ) ]]; then
   echo $0: Error: CWD does not look like the root of an Android tree. > /dev/stderr
   exit 1
fi

if [[ -f .repo/manifest.xml ]] ; then
   mergeBranch=$(repo info -l .repo/manifest 2>/dev/null | sed -ne 's/^Manifest merge branch: \(.*\)$/\1/p')

   # Drop 'ref/tags/' if present
   mergeBranch=$(basename $mergeBranch)

   # Check for an AU tag and extract the corresponding branch name
   mergeBranch=${mergeBranch#AU_LINUX_GECKO_}
   mergeBranch=${mergeBranch%.[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9].[0-9][0-9][0-9]}

   # Old-style branch names are lowercase
   if [[ $mergeBranch =~ ^B2G_ ]]; then
     mergeBranch=${mergeBranch,,}
   fi

   # Drop b2g_ prefix from old-style branch names
   mergeBranch=${mergeBranch#b2g_}

   echo $mergeBranch all
else
   echo all
fi

